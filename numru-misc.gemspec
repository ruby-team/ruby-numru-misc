# coding: utf-8
require "rubygems" unless defined?(Gem)

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "numru/misc"

Gem::Specification.new do |spec|
  spec.name          = "numru-misc"
  spec.version       = NumRu::Misc::VERSION
  spec.authors     = ["Takeshi Horinouchi"]
  spec.email       = ["horinout@gfd-dennou.org"]

  spec.summary     = %q{Collection of miscellaneous functions and classes to facilitate programming.}
  spec.description = %q{Miscellaneous functions and classes to help Ruby programming. To be used in other NumRu libraries.}
  spec.homepage    = 'http://www.gfd-dennou.org/arch/ruby/products/numru-misc/'
  spec.licenses       = ["BSD-2-Clause"]

  spec.files         = [".ChangeLog.until201414",".gitignore","Gemfile","LICENSE.txt","Rakefile","debian/changelog","debian/compat","debian/control","debian/copyright","debian/ruby-numru-misc.doc-base","debian/ruby-numru-misc.docs","debian/ruby-numru-misc.links","debian/rules","debian/source/format","debian/watch","doc/emath.html","doc/index.html","doc/keywordopt.html","doc/md_iterators.html","doc/misc.html","install.rb","lib/numru/misc.rb","lib/numru/misc/emath.rb","lib/numru/misc/keywordopt.rb","lib/numru/misc/md_iterators.rb","lib/numru/misc/misc.rb","lib/numru/misc/version.rb","makedoc.csh","numru-misc.gemspec"]
  #spec.test_files    = spec.files.grep(%r{^demo/})
  spec.require_paths = ["lib"]

  spec.required_ruby_version = Gem::Requirement.new(">= 1.6")
  spec.add_runtime_dependency(%q<narray>, [">= 0.5.5"])
end
